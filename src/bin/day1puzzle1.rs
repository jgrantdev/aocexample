mod aochelper;

fn main() {
    println!("Solving puzzle 1");
    aochelper::load_file("day1input.txt");
}
