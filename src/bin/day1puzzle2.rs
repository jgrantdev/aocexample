mod aochelper;

fn main() {
    println!("Solving puzzle 2");
    aochelper::load_file("day1input.txt");

    let answer = add_numbers(10, 20);
    println!("The answer to puzzle 2 is {}", answer);
}

fn add_numbers(a: usize, b: usize) -> usize {
    a + b
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_examples() {
        let examples = [
            (1, 2, 3),
            (5, 1, 6),
            (100, 200, 300),
        ];

        for example in examples.iter() {
            assert_eq!(add_numbers(example.0, example.1), example.2);
        }
    }
}